package com.ets.file.manager.model.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenericResponseObject {
	
	private Response response;
	private Object result;
	
	
	

}
