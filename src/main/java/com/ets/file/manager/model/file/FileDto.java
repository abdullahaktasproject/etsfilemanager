package com.ets.file.manager.model.file;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

@ApiModel(value = "FileDto Api model documentation", description = "Model")
public class FileDto {
	
	@ApiModelProperty(value = "Unique id field of file object")
	private Long id;
	
	@ApiModelProperty(value = "File read path ")
	private String path;
	
	@ApiModelProperty(value = "File name value of file object ")
	private String name;
	
	@ApiModelProperty(value = "File base64 data")
	private String data;
	
	@ApiModelProperty(value = "Extension of file object")
	private String extension;
	
	@ApiModelProperty(value = "Size of file object")
	private String size;
	
	
	public String getBase64CoreData() {
		String[] base64Data = data.split(",");
		return base64Data.length == 1 ? base64Data[0] : base64Data[1];
	}

	public String getFileInfoData() {
		String[] base64Data = data.split(",");
		return base64Data.length >= 1 ? base64Data[0] : base64Data[1];
	}
	
	

}
