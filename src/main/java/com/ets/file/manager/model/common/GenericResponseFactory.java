package com.ets.file.manager.model.common;

import java.util.concurrent.ConcurrentHashMap;

import com.ets.file.manager.enums.ErrorCodesEnum;

public class GenericResponseFactory {
	
	private static ConcurrentHashMap<ErrorCodesEnum, Response> responseMap = new ConcurrentHashMap<>();

	public static GenericResponseObject success(Object result) {
		var response = generateResponse(ErrorCodesEnum.SUCCESS);
		return new GenericResponseObject(response, result);
	}

	public static GenericResponseObject success(Object result, String customMessage) {
		var response = generateResponse(ErrorCodesEnum.SUCCESS);
		response.setErrorMsg(customMessage);
		return new GenericResponseObject(response, result);
	}

	
	public static GenericResponseObject generate(ErrorCodesEnum errorCodesEnum) {
		var response = generateResponse(errorCodesEnum);
		return new GenericResponseObject(response, null);
	}
	

	private static Response generateResponse(ErrorCodesEnum errorCodesEnum) {
		Response response;
		if(responseMap.containsKey(errorCodesEnum)) {
			response = responseMap.get(errorCodesEnum);
		}else {
			response = new Response(errorCodesEnum.getCode(), errorCodesEnum.getMessage());
		}
		
		return response;
	}
}
