package com.ets.file.manager.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.ets.file.manager.enums.ErrorCodesEnum;
import com.ets.file.manager.exceptions.AppException;
import com.ets.file.manager.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private UserRepository userRepository;
	
	 @Override
	    public UserDetails loadUserByUsername(String username) throws AppException {
	        Optional<com.ets.file.manager.entity.User> user = userRepository.findByUserName(username);
	        if (user.isEmpty()) {
	            throw new AppException(ErrorCodesEnum.LOGIN_ERROR);
	        }
	        return new User(user.get().getUserName(), user.get().getPass(), new ArrayList<>());
	    }
}
