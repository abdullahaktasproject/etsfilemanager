package com.ets.file.manager.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ets.file.manager.entity.File;
import com.ets.file.manager.enums.ErrorCodesEnum;
import com.ets.file.manager.exceptions.AppException;
import com.ets.file.manager.model.common.GenericResponseFactory;
import com.ets.file.manager.model.common.GenericResponseObject;
import com.ets.file.manager.model.file.FileDto;
import com.ets.file.manager.repository.FileRepository;
import com.ets.file.manager.utils.SizeConvertUtil;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class FileService {
	
	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private FileOperationService fileOperationService;
	
	
	public GenericResponseObject getFiles() {
		Iterable<File> files = fileRepository.findAll();
		
		return GenericResponseFactory.success(files);
	}

	public GenericResponseObject saveFile(FileDto fileDto) {
		
		
		String size;
		String path;
		
		try {
			fileOperationService.checkExtension(fileDto.getExtension());
			size = SizeConvertUtil.bytesToMb(fileDto.getData());
			path = fileOperationService.saveFileOnDisk(fileDto);
		} catch (AppException e) {
			log.error("app exception ", e);
			return GenericResponseFactory.generate(e.getError());
		} catch (IOException e) {
			log.error("io exception : ", e);
			return GenericResponseFactory.generate(ErrorCodesEnum.UNEXPECTED_ERROR);
		}
		
		File file = new File();
		file.setExtension(fileDto.getExtension());
		file.setName(fileDto.getName());
		file.setSize(size);
		file.setPath(path);
		
		fileRepository.save(file);
		
		return GenericResponseFactory.success("");
	}

	public GenericResponseObject deleteFile(FileDto fileDto) {
		
		Optional<File> file =  fileRepository.findById(fileDto.getId());
		
		file.ifPresentOrElse(
				f -> {
						fileOperationService.deleteFileOnDisk(f);
						fileRepository.delete(f);
						}, () -> { throw new AppException(ErrorCodesEnum.FILE_NOT_FOUND);});
		
		return GenericResponseFactory.success(null,"Dosya başarıyla silindi");
	}


	
	public InputStream getFile(long id) {
		
		Optional<File> file =  fileRepository.findById(id);
		
		InputStream in = null;
		
		if(file.isPresent()) {
			try {
				in =new FileInputStream(file.get().getPath());
			} catch (FileNotFoundException e) {
				log.error("getFile error : ", e);
			}
		}else { 
			throw new AppException(ErrorCodesEnum.FILE_NOT_FOUND);
		};
		
		return in;
	}
	

}
