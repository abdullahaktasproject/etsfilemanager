package com.ets.file.manager.services;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;

import com.ets.file.manager.constants.AppConstant;
import com.ets.file.manager.entity.File;
import com.ets.file.manager.enums.ErrorCodesEnum;
import com.ets.file.manager.exceptions.AppException;
import com.ets.file.manager.model.file.FileDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FileOperationService {
	
	private static List<String> extensionList;
	
	
	@PostConstruct
	private void initExtensionList() {
		extensionList = new ArrayList<>();
		extensionList.add("png");
		extensionList.add("jpeg");
		extensionList.add("jpg");
		extensionList.add("docx");
		extensionList.add("pdf");
		extensionList.add("xlsx");
	}
	
	public String saveFileOnDisk(FileDto fileDto) throws IOException {

		String fullPath = AppConstant.FILE_PATH + "\\files\\" + fileDto.getName() + "." + fileDto.getExtension();
		
		byte[] data = Base64.decodeBase64(fileDto.getBase64CoreData());
		Path destinationFile = Paths.get(fullPath);
		Files.createDirectories(destinationFile.getParent());
		Files.write(destinationFile, data);
		
		return fullPath;
	}

	public void deleteFileOnDisk(File file)  {
			Path destinationFile = Paths.get(file.getPath());
			try {
				Files.delete(destinationFile);
			} catch (IOException e) {
				log.error("deleteFileOnDisk error : ", e);
			}
	}
	
	
	public void checkExtension(String extension) throws AppException{
		extensionList.stream().filter(ext -> ext.equalsIgnoreCase(extension)).findFirst().orElseThrow(() -> new AppException(ErrorCodesEnum.EXTENSION_ERROR));
	}



}
