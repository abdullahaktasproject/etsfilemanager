package com.ets.file.manager.constants;

public class AppConstant {
	
	public static final String SECRET = "EtsCase";
    public static final long EXPIRATION_TIME = 423_000_000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/login";
    public static final String FILE_PATH = System.getProperty("user.home");
    public static final long FILE_MAX_SIZE = 5;

}
