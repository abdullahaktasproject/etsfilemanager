package com.ets.file.manager.enums;

import lombok.Getter;

public enum ErrorCodesEnum {
	SUCCESS("0","İşlem başarılı"),
	UNEXPECTED_ERROR("99","Beklenmedik bir hata oluştu"),
	BIG_SIZE_ERROR("101","Dosya boyutu max 5mb olmalıdır."),
	LOGIN_ERROR("102","Hatalı kullanıcı adı veya şifre"),
	EXTENSION_ERROR("103","Geçerli uzantıda dosya yükleyiniz."),
	FILE_NOT_FOUND("104","Dosya bulunamadı");
	
	
	@Getter
	private String code;
	@Getter
	private String message;

	ErrorCodesEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}
}
