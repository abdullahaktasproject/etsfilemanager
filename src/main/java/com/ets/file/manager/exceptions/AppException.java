package com.ets.file.manager.exceptions;

import com.ets.file.manager.enums.ErrorCodesEnum;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AppException extends RuntimeException{
	
	@Getter
	private ErrorCodesEnum error;

	/**
	 * 
	 */
	private static final long serialVersionUID = 6245299254599409978L;
	
	public AppException(ErrorCodesEnum errorCodesEnum) {
		super(errorCodesEnum.getMessage());
		this.error = errorCodesEnum;
	}

}
