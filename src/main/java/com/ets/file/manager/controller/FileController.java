package com.ets.file.manager.controller;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ets.file.manager.model.common.GenericResponseObject;
import com.ets.file.manager.model.file.FileDto;
import com.ets.file.manager.services.FileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@CrossOrigin
@Api(value = "File Api documentation")
public class FileController {
	
	@Autowired
	private FileService fileService;

	@ResponseBody
	@GetMapping(value = "/get-files")
	@ApiOperation(value = "Get all files info method")
	public GenericResponseObject getFiles() {
		return fileService.getFiles();
	}

	@ResponseBody
	@ApiOperation(value = "Save file method")
	@PostMapping(value = "/save-file", produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponseObject saveFile(@RequestBody FileDto request) {
		return fileService.saveFile(request);
	}

	@ResponseBody
	@ApiOperation(value = "Delete file method")
	@PostMapping(value = "/delete-file", produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponseObject deleteFile(@RequestBody FileDto request) {
		return fileService.deleteFile(request);
	}
	
	@ApiOperation(value = "Get file byte[] method")
	@GetMapping(value = "/get-file/{id}")
	public @ResponseBody byte[] getImage(@PathVariable("id") long id) throws IOException {
	    InputStream in = fileService.getFile(id);
	    return IOUtils.toByteArray(in);
	}

}
