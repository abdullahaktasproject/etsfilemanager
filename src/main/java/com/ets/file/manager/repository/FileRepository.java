package com.ets.file.manager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ets.file.manager.entity.File;


@Repository
public interface FileRepository extends CrudRepository<File,Long>{
	

}
