package com.ets.file.manager.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ets.file.manager.entity.User;


@Repository
public interface UserRepository extends CrudRepository<User,Long>{
	
	Optional<User> findByUserNameAndPass(String userName, String Pass);
	Optional<User> findByUserName(String userName);

}
