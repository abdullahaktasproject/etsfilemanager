package com.ets.file.manager.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "users")
public class User implements Serializable{

	@Id
    @GeneratedValue
    private Long id; 
	
	@Column(unique=true)
	private String userName;
	
	private String pass;
}
