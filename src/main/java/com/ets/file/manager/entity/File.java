package com.ets.file.manager.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "files")
public class File implements Serializable{

	@Id
    @GeneratedValue
    private Long id; 
	private String name;
	private String path;
	private String size;
	private String extension;
}
