package com.ets.file.manager.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.ets.file.manager.constants.AppConstant;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authenticationManager;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) {
		try {
			com.ets.file.manager.entity.User creds = new ObjectMapper().readValue(req.getInputStream(),
					com.ets.file.manager.entity.User.class);

			return authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(creds.getUserName(), creds.getPass(), new ArrayList<>()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {

		String token = Jwts.builder().setSubject(((User) auth.getPrincipal()).getUsername())
				.setExpiration(new Date(System.currentTimeMillis() + AppConstant.EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, AppConstant.SECRET.getBytes()).compact();
		res.addHeader(AppConstant.HEADER_STRING, AppConstant.TOKEN_PREFIX + token);
		res.addHeader("Access-Control-Expose-Headers", "Authorization");
	}
}
