package com.ets.file.manager.utils;

import com.ets.file.manager.constants.AppConstant;
import com.ets.file.manager.enums.ErrorCodesEnum;
import com.ets.file.manager.exceptions.AppException;

public class SizeConvertUtil {
	
	
	public static String bytesToMb(String data) throws AppException {
		var bytes = 4 * Math.ceil((data.length() / 3))*0.5624896334383812;
		
	    long kilobyte = 1024;
	    long megabyte = kilobyte * 1024;
	    long gigabyte = megabyte * 1024;

	    if ((bytes >= 0) && (bytes < kilobyte)) {
	        return bytes + " B";

	    } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
	        return (bytes / kilobyte) + " KB";

	    } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
	    	if((bytes / megabyte) > AppConstant.FILE_MAX_SIZE) {
	    		throw new AppException(ErrorCodesEnum.BIG_SIZE_ERROR);
	    	}
	        return (bytes / megabyte) + " MB";

	    } else {
	        return bytes + " Bytes";
	    }
	}

}
